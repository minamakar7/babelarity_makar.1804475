package it.uniroma1.lcl.babelarity;

import java.io.Serializable;

/**
 * Classe Document 
 * @author Mina
 *
 */
public class Document implements LinguisticObject, Serializable {
	private static final long serialVersionUID = 1L;
	private String ID;
	private String title;
	private String content;
	
	/**
	 * Costruttore del word
	 * @param iD ID documento
	 * @param titelo del documento
	 * @param contenuto del documento
	 */
	public Document(String iD, String titelo, String contenuto) {
		this.ID = iD;
		this.title = titelo;
		this.content = contenuto;
	}
	/**
	 * Restituisce ID del documento
	 * @return ID del documento
	 */
	public String getId() {
		return ID;
	}
	/**
	 * Restituisce titolo del documento
	 * @return titolo del documento
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Restituisce contenuto del documento
	 * @return contenuto del documento
	 */
	public String getContent() {
		return content;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (obj instanceof Document) {
			Document d = (Document) obj;
			return d.ID.equals(this.ID) && this.title.equals(d.title) && this.content.equals(d.content);
		}
		return false;
	}


}
