package it.uniroma1.lcl.babelarity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
/**
 * Contiene l'id, Part of Speech, le definizioni del synset,
 * le lessicalizzazioni di cui e' composto il synset e le relazioni
 * @author Mina
 *
 */
public class BabelSynset implements Synset {
	private String ID;
	private POS POS;
	private Set<String> lemmas;
	private Set<String> glosses;
	private List<Relazione> relations=new ArrayList<>();
	/**
	 * Costruttore del BabelSynset
	 * @param ID l'id univoco del synset sotto forma di stringa
	 * @param POS la parte del discorso (Part of Speech) del synset
	 * @param lemmas  l’insieme delle lessicalizzazioni di cui è composto il synset
	 * @param glosses le definizioni del synset
	 */
	public BabelSynset(String ID, POS POS, Set<String> lemmas, Set<String> glosses) {
		this.ID = ID;
		this.POS = POS;
		this.lemmas = lemmas;
		this.glosses = glosses;
	}
	
	@Override
	public String getID() {
		return ID;
	}
	
	@Override
	public POS getPOS() {
		return POS;
	}

	@Override
	public  Set<String> getLemmas() {
		return lemmas;
	}

	@Override
	public Set<String> getGlosses() {
		return glosses;
	}
	
	@Override
	public void addRelation(Relazione r) {
		relations.add(r);
	}
	
	@Override
	public String toString() {
		String lemmi = lemmas.stream().collect(Collectors.joining(";"));
		String glosse = glosses.stream().collect(Collectors.joining(";"));
		String relazioni = relations.stream().map(rel -> rel.getDestinatario().getID()+"_"+rel.getSemplice()).collect(Collectors.joining(";"));
		return ID + "\t" + POS + "\t" + lemmi + "\t" + glosse + "\t" + relazioni;
	}
	
	@Override
	public List<Relazione> getRelations() {
		return relations;
	}
}	
