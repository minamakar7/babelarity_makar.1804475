package it.uniroma1.lcl.babelarity;

import java.nio.file.Path;
import java.nio.file.Paths;
/**
 * Contiene i file 
 * @author Mina
 *
 */
public enum FileName {
	DICTIONARY("resources/dictionary.txt"),
	LEMMA("resources/lemmatization-en.txt"),
	GLOSSES("resources/glosses.txt"),
	RELATION("resources/relations.txt"),
	STOPWORDS("stopwords.txt"),
	CORPUS("resources/corpus/");
	
	private String fileName;
	
	private FileName(String s) {
		fileName = s; 
	}
	/**
	 * Restituisce il nome del file
	 * @return il nome del file
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * Restituisce il percorso del file
	 * @return il percorso del file
	 */
	public Path getPath() {
		return Paths.get(fileName);
	}
}
