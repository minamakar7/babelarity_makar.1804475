package it.uniroma1.lcl.babelarity;

/**
 * Classe che rappresenta una relazione
 * @author Mina
 *
 */
public class Relazione {
	private Synset sorgente;
	private Synset destinatario;
	private String semplice;
	private String completa;
	
	/**
	 * Costruttore standard di Relazione che contiene synset sorgent,destinatario, nome relazione semplice
	 * e nome relazione completo
	 * @param sorgente Synset sorgente
	 * @param destinatario Synset
	 * @param semplice nome relazione semplice
	 * @param completa nome relazione completo
	 */
	public Relazione(Synset sorgente, Synset destinatario, String semplice, String completa) {
		this.sorgente = sorgente;
		this.destinatario = destinatario;
		this.semplice = semplice;
		this.completa = completa;
	}
	
	/**
	 * resituisce il synset sorgente
	 * @return il synset sorgente
	 */
	public Synset getSorgente() {
		return sorgente;
	}

	/**
	 * Restituisce il sysnet destinatario
	 * @return il sysnet destinatario
	 */
	public Synset getDestinatario() {
		return destinatario;
	}
	
	/**
	 * Restituisce il nome relazione semplice 
	 * @return il nome relazione semplice 
	 */
	public String getSemplice() {
		return semplice;
	}

	/**
	 * Restituisce il nome relazione completo
	 * @return il nome relazione completo
	 */
	public String getCompleta() {
		return completa;
	}
	
	@Override
	public String toString() {
		return sorgente + ";" + destinatario + ";" + semplice;
	}
}
