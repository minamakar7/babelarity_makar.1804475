package it.uniroma1.lcl.babelarity;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
/**
 * La similarita' semantica tra due synset 
 * @author Mina
 *
 */
public class BabelSemanticSimilarity implements BabelSimilarity {

	@Override
	public double computeSimilarity(LinguisticObject o1, LinguisticObject o2) {
		Synset s1= (Synset)o1;
		Synset s2= (Synset)o2;
		return 1.0 / (BFS(s1, s2) + 1);
	}
	/**
	 * Calcola il cammino tra due synset 
	 * @param sorgente Synset sorgente 
	 * @param destinatario Synset destinatario
	 * @return ritorna la lunghezza del cammino
	 */
	private int BFS(Synset sorgente, Synset destinatario) {
		LinkedList<Synset> Q = new LinkedList<>();
		Set<Synset> marcati = new HashSet<>();
		Q.add(sorgente);
		marcati.add(sorgente);
		int conta = 0;
		int figli=1;
		int fNext=0;
		while (Q.size() != 0) {
			Synset t = Q.get(0);
			Q.remove(t);
			if (t == destinatario) 
				return conta;
			List<Relazione> rels = t.getRelations();
			for (Relazione relazione : rels) {
				String vicinoID = relazione.getDestinatario().getID();
				Synset u = MiniBabelNet.getInstance().getSynset(vicinoID);
				if (!marcati.contains(u)) {
					marcati.add(u);
					Q.add(u);
				}
			}
			fNext+=rels.size();
			figli--;
			if (figli==0) {
				conta++;
				figli=fNext;
				fNext=0;
			}
		}
		throw new RuntimeException("Il destinatario non si trova sul grafo");
	}
	
}
