package it.uniroma1.lcl.babelarity;
/**
 * Enum che definisice i vari POS
 * @author Mina
 */
public enum POS {
	NOUN, ADV, ADJ, VERB;
	
	/**
	 * Estrae e converte il POS in un id del synset
	 * @param id del synset
	 * @return Pos corrispondente
	 * @throws POSException
	 */
	public static POS getPosfromID(String id) throws POSException {
		return getPOSfromChar(id.charAt(id.length()-1));
	}
	
	/**
	 * Converte un carattere in un POS
	 * @param c carattere
	 * @return POS corrispondente
	 * @throws POSException
	 */
	public static POS getPOSfromChar(char c) throws POSException {
		switch(c) {
			case 'n': return NOUN;
			case 'v': return ADV;
			case 'a': return ADJ;
			case 'r': return VERB;
			default: throw new POSException();
		}
	}
}
/**
 * Exception conversione pos
 * @author Mina
 *
 */
class POSException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public POSException() {
		super("POS errato");
	}
}
