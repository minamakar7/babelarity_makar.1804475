package it.uniroma1.lcl.babelarity;

import java.util.List;
import java.util.Set;

/**
 * Contiene al suo interno ID
 * 
 * @author Mina
 *
 */
public interface Synset extends LinguisticObject {
	/**
	 * Restituisce l'id univoco del synset sotto forma di stringa
	 * 
	 * @return l'id univoco del synset sotto forma di stringa
	 */
	String getID();

	/**
	 * Restituisce la parte del discorso (Part-of-Speech) del synset
	 * 
	 * @return la parte del discorso (Part-of-Speech) del synset
	 */
	POS getPOS();

	/**
	 * Restituisce un insieme di lemmi
	 * @return  un insieme di lemmi
	 */
	Set<String> getLemmas();
	/**
	 * Restituisce un insieme di glosses
	 * @return un insieme di glosses
	 */
	Set<String> getGlosses();
	/**
	 * Restituiusce un insieme di relazioni
	 * @return un insieme di relazioni
	 */
	List<Relazione> getRelations();
	/**
	 * Aggiunge alla classe synset la relazione corrispondente
	 * @param relation la relazione da inserire
	 */
	void addRelation(Relazione relation);
}
