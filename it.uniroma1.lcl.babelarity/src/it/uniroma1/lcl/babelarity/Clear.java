package it.uniroma1.lcl.babelarity;

import java.util.Set;


/**
 * La classe clear si occupa della pulizia di un testo
 * @author Mina
 *
 */
public class Clear {
	private static StringBuilder stringBuilder= new StringBuilder();
	/**
	 * Si occupa di pulire un testo, cancellando tutte le stopwords e le lettere non alfabetiche
	 * @param parole il set dove verrano aggiunte le parole
	 * @param testo la stringa da pulire
	 * @param stopwords le stopwords
	 */
	public static void clears(Set<String> parole, String testo, Set<String> stopwords) {
		MiniBabelNet babelNet=MiniBabelNet.getInstance();
		int n = testo.length();
		for (int i = 0; i < n; i++) {
			char character = testo.charAt(i);
			if(character >= 'a' && character <='z' || character == '-') stringBuilder.append(character);
			else {
				String s = stringBuilder.toString();
				if (stringBuilder.length() != 0 && !stopwords.contains(s)) {
					String lemma = babelNet.getLemmas(s);
					if (lemma != null) parole.add(lemma);
				}
				stringBuilder.setLength(0);
				stringBuilder.trimToSize();
			}
		}
		if (stringBuilder.length() != 0) {
			String s = stringBuilder.toString();
			String lemma = babelNet.getLemmas(s);
			if (lemma != null) parole.add(lemma);
		}
		stringBuilder.setLength(0);
		stringBuilder.trimToSize();
	}
}

