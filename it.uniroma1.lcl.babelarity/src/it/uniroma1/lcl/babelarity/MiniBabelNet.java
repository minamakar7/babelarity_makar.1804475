package it.uniroma1.lcl.babelarity;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Classe principale della semantica
 * 
 * @author Mina
 *
 */
public class MiniBabelNet implements Iterable<Synset>, BabelSimilarity {

	private static MiniBabelNet instance = new MiniBabelNet();

	private Map<String, String> mappaLemma;

	private Set<String> lemmi = new HashSet<String>();

	private Map<String, Synset> idToSynset = new HashMap<>();

	/**
	 * Mantiene tutti i documenti caricati
	 */
	protected static Set<Set<String>> documenti = null;

	private BabelSimilarity bs;

	private Map<String, List<Relazione>> mappaRel;

	/**
	 * Contiene tutte le stopwords
	 */
	public static Set<String> stopwords;

	private MiniBabelNet() {
		stopwords = FileLoader.load(FileName.STOPWORDS, new HashSet<>(), FileLoader::stopwords);
		mappaLemma = FileLoader.load(FileName.LEMMA, new HashMap<>(), FileLoader::lemma);

		mappaLemma.values().stream().forEach(lemma -> lemmi.add(lemma));

		Map<String, Set<String>> mappaDiz = FileLoader.load(FileName.DICTIONARY, new HashMap<>(),
				FileLoader::dictionary);
		Map<String, Set<String>> mappaGloss = FileLoader.load(FileName.GLOSSES, new HashMap<>(), FileLoader::gloss);
		mappaDiz.keySet().stream().forEach(s -> {
			try {
				idToSynset.put(s, new BabelSynset(s, POS.getPosfromID(s), mappaDiz.get(s), mappaGloss.get(s)));
			} catch (POSException e) {
				e.printStackTrace();
			}
		});
		mappaRel = FileLoader.load(FileName.RELATION, new HashMap<>(), FileLoader::relazioni, idToSynset);
		mappaDiz.clear();
		mappaGloss.clear();
	}

	/**
	 * Restituisce l'istanza di MiniBabelNet
	 * 
	 * @return l'istanza di MiniBabelNet
	 */
	public static MiniBabelNet getInstance() {
		return instance;
	}

	/**
	 * Restituisce la mappa dei lemmi
	 * 
	 * @return la mappa dei lemmi
	 */
	public Map<String, String> getMappaLemma() {
		return mappaLemma;
	}

	/**
	 * Restituisce tutte le relazioni
	 * 
	 * @return tutte le relazioni
	 */
	public Map<String, List<Relazione>> getAllRelations() {
		return mappaRel;
	}

	/**
	 * Restituisce l'insieme di synset che contengono tra i loro sensi la parola in
	 * input
	 * 
	 * @param word parola associata ai synset
	 * @return l'insieme di synset che contengono tra i loro sensi la parola in
	 *         input
	 */
	public Set<Synset> getSynsets(String word) {
		return idToSynset.values().stream().filter(x -> x.getLemmas().contains(word)).collect(Collectors.toSet());
	}

	/**
	 * Restituisce il synset relativo all'id specificato
	 * 
	 * @param id l'identificatore dei synset
	 * @return il synset relativo all'id specificato
	 */
	public Synset getSynset(String id) {
		return idToSynset.get(id);
	}

	/**
	 * Restituisce il lemma associata alla parola flessa fornita in input
	 * 
	 * @param word la parola flessa o lemma stesso da vedere se lemma
	 * @return il lemma associata alla parola flessa fornita in input
	 */
	public String getLemmas(String word) {
		if (mappaLemma.containsKey(word))
			return mappaLemma.get(word);
		return lemmi.contains(word) ? word : null;
	}

	/**
	 * Restituisce le informazioni inerenti al Synset fornito in input sotto forma
	 * di stringa
	 * 
	 * @param synset del quale si vuole ottenere le informazioni
	 * @return le informazioni inerenti al Synset fornito in input sotto forma di
	 *         stringa
	 */
	public String getSynsetSummary(Synset synset) {
		return synset.toString();
	}

	@Override
	public double computeSimilarity(LinguisticObject o1, LinguisticObject o2) {
		if (o1 instanceof Word) {
			setLexicalSimilarityStrategy();
			return bs.computeSimilarity((Word) o1, (Word) o2);
		} else if (o1 instanceof Document) {
			setDocumentSimilarityStrategy();
			return bs.computeSimilarity((Document) o1, (Document) o2);
		}
		setSemanticSimilarityStrategy();
		return bs.computeSimilarity((Synset) o1, (Synset) o2);
	}

	/**
	 * Imposta l'algoritmo di calcolo della similarita' tra parole
	 */
	public void setLexicalSimilarityStrategy() {
		bs = new BabelLexicalSimilarity();
	}

	/**
	 * Imposta l'algoritmo di calcolo della similarita' tra documenti
	 */
	public void setDocumentSimilarityStrategy() {
		bs = new BabelDocumentSimilarity();
	}

	/**
	 * Imposta l'algoritmo di calcolo della similarita' tra synset
	 */
	public void setSemanticSimilarityStrategy() {
		bs = new BabelSemanticSimilarity();
	}

	/**
	 * Restiusce gli attributi della mappa idToSynset sotto forma di collection
	 * 
	 * @return gli attributi della mappa idToSynset sotto forma di collection
	 */
	public Collection<Synset> getSynsets() {
		return idToSynset.values();
	}

	@Override
	public Iterator<Synset> iterator() {
		return idToSynset.values().iterator();
	}
	/**
	 * Calcola la cosineSimilarity tra due mappe che contengono i valore a cui tocca applicare l'algoritmo 
	 * @param mappa1 Vettore delle occorenze dopo aver usato il pmi
	 * @param mappa2 Vettore delle occorenze dopo aver usato il pmi
	 * @return il valore della similarita'
	 */
	public double cosineSimilarity(Map<String,Double> mappa1,Map<String,Double> mappa2) {
		double valore=0;
		valore = mappa1.keySet().stream().filter(x->mappa2.containsKey(x)).mapToDouble(x->mappa1.get(x)*mappa2.get(x)).sum();
		double valueVettorepot1 = mappa1.values().stream().mapToDouble(x -> x.doubleValue() * x.doubleValue()).sum();
		double valueVettorepot2 = mappa2.values().stream().mapToDouble(x -> x.doubleValue() * x.doubleValue()).sum();
		double risultato = (valore)/(Math.sqrt(valueVettorepot1)*Math.sqrt(valueVettorepot2)); 
		return Math.round(risultato * 100.0) / 100.0;
	}

}