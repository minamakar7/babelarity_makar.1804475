package it.uniroma1.lcl.babelarity;
/**
 * Un tipo di linguisticObject
 * @author Mina
 *
 */
public class Word implements LinguisticObject{
	private String parola;
	/**
	 * Costruttore di word
	 * @param parola da inserire nella classe word
	 */
	private Word(String parola) {
		this.parola = parola.toLowerCase();
	}
	/**
	 * Restituisce la parola
	 * @return la parola
	 */
	public String getParola() {
		return parola;
	}
	/**
	 * Ritorna l'oggetto word
	 * @param parola Stringa 
	 * @return l'oggetto word
	 */
	public static Word fromString(String parola) {
		return new Word(parola);
	}
	
	@Override
	public String toString() {
		return parola;
	}

	
}
