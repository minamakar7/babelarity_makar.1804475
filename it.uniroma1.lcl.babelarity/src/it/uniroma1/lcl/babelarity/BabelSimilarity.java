package it.uniroma1.lcl.babelarity;

/**
 * Si occupa di effettuare la similarita' tra oggetti linguistici
 * @author Mina
 *
 */
@FunctionalInterface
public interface BabelSimilarity {
	/**
	 * Calcola e restituisce un double che rappresenta
	 * la similarita' tra due oggetti linguistici (Synset, Documenti o parole)
	 * @param o1 oggetto linguistico che sia synset document o parole
	 * @param o2 oggetto linguistico che sia synset document o parole
	 * @return la similarita' tra due oggetti linguistici
	 */
	double computeSimilarity(LinguisticObject o1, LinguisticObject o2);
}
