package it.uniroma1.lcl.babelarity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
/**
 * Si occupa della similarita' tra due documenti
 * @author Mina
 *
 */
public class BabelDocumentSimilarity implements BabelSimilarity {

	@Override
	public double computeSimilarity(LinguisticObject o1, LinguisticObject o2) {
		Document d1= (Document)o1;
		Document d2= (Document)o2;
		MiniBabelNet babelNet = MiniBabelNet.getInstance();
		Set<String> doc1 = new HashSet<>();
		Set<String> doc2 = new HashSet<>();
		Set<String> IDdoc1 = new HashSet<>(); 
		Set<String> IDdoc2 = new HashSet<>();
		Clear.clears(doc1, d1.getContent(), MiniBabelNet.stopwords);
		Clear.clears(doc2, d2.getContent(), MiniBabelNet.stopwords);
		Set<Synset> synsetDoc1 = new HashSet<>();
		Set<Synset> synsetDoc2 = new HashSet<>();
		doc1.forEach(x -> {
			Set<Synset> sys=babelNet.getSynsets(x);
			synsetDoc1.addAll(sys);
			for (Synset synset:sys) IDdoc1.add(synset.getID());
		});
		doc2.forEach(x ->{
			Set<Synset> sys=babelNet.getSynsets(x);
			synsetDoc2.addAll(babelNet.getSynsets(x));
			for (Synset synset:sys) IDdoc2.add(synset.getID());
		});
		Map<Synset,List<Synset>>grafoDoc1 = creaGrafo(synsetDoc1, IDdoc1, babelNet);
		List<Synset> listaSys=grafoDoc1.keySet().stream().collect(Collectors.toList());
		Map<Synset,List<Synset>>grafoDoc2=creaGrafo(synsetDoc2, IDdoc2, babelNet);
		List<Synset> listaSys2=grafoDoc2.keySet().stream().collect(Collectors.toList());
		Map<String,Double> mappa1=graph(listaSys, grafoDoc1, 10000);
		Map<String,Double> mappa2=graph(listaSys2, grafoDoc2, 10000);
		return babelNet.cosineSimilarity(mappa1, mappa2);
	}
	
	/**
	 * Se due nodi del grafo sono in relazione diretta si aggiunge un arco e restituisce 
	 * un booleano a seconda se l'ha trovato o meno
	 * @param grafoDoc grafo del documento
	 * @param s Synset
	 * @param IDdoc Tutti gli ID dei synset del documento
	 * @param lista delle relazioni di un sysnet
	 * @return un booleano a seconda se l'ha trovato o meno
	 */
	public static boolean creaArchi(Map<Synset,List<Synset>> grafoDoc,Synset s,Set<String> IDdoc,List<Relazione> lista) {
		boolean b=false;
		for(Relazione r:lista) 
			if (IDdoc.contains(r.getDestinatario().getID())) {
				b=true;
				if(grafoDoc.containsKey(s)) grafoDoc.get(s).add(r.getDestinatario());
				else {
					List<Synset> listaRis = new ArrayList<>();
					listaRis.add(r.getDestinatario());
					grafoDoc.put(s, listaRis);
					
				}
			}
		return b;
	}
	/**
	 * Crea un grafo partendo da un insieme  di synset rispetto alle loro relazioni
	 * @param synsetDoc Tutti i synset del documento
	 * @param IDdoc Tutti gli ID dei synset del documento
	 * @param babelNet istanza di MiniBabelNet
	 * @return restituisce il grafo del documento
	 */
	public static Map<Synset, List<Synset>> creaGrafo(Set<Synset> synsetDoc,Set<String> IDdoc,MiniBabelNet babelNet){
		Map<Synset, List<Synset>>grafoDoc =new HashMap<>();
		for (Synset s : synsetDoc) 
			if (s.getRelations() != null) {
				boolean b = creaArchi(grafoDoc, s, IDdoc,s.getRelations());
				if (!b) 
					for(Relazione r:s.getRelations()) {
						List<Relazione> rel = r.getDestinatario().getRelations();
						if (rel != null) creaArchi(grafoDoc, s, IDdoc, rel);
						
					}
			}
		return grafoDoc;
	}
	/**
	 * Esegue l'algoritmo RandomWalk
	 * @param V lista dei synset
	 * @param grafoDoc2 grafo del documento
	 * @param k numero di iterazione
	 * @return restituisce la mappa avente l'id del synset e il numero di volte che e' stato visitato
	 */
	public static Map<String, Double> graph(List<Synset> V, Map<Synset, List<Synset>> grafoDoc2,int k) {
		MiniBabelNet bn = MiniBabelNet.getInstance();
		Random random = new Random();
		Map<String,Double>  mappa= new HashMap<>();
		Collection<Synset> b = bn.getSynsets();
		b.forEach(x->mappa.put(x.getID(),0.0));
		int posRandomSynset = random.nextInt(V.size()-1);
		Synset n = V.get(posRandomSynset);
		for (int i=0;i<k;i++) {
			double rand=Math.random();
			if (rand > 0.3) {
				posRandomSynset = random.nextInt(V.size()-1);
				n = V.get(posRandomSynset);
			}
			mappa.merge(n.getID(), 1.0, (oldd,neww) -> oldd+1);
			List<Synset> neighbours = grafoDoc2.get(n);
			if(neighbours==null) { 
				posRandomSynset = random.nextInt(V.size()-1);
				n = V.get(posRandomSynset);
			}
			else
			{
				Synset next_step = neighbours.get(random.nextInt(neighbours.size()));
				n = next_step;
			}
		}
		return mappa;
	}

}
