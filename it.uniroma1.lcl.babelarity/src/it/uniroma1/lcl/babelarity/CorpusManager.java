package it.uniroma1.lcl.babelarity;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * CorpusManager si occupa del caricamento e Salvataggio dei file
 * 
 * @author Mina
 *
 */
public class CorpusManager implements Iterable<Document> {
	private static CorpusManager instance;
	private Set<Document> documents = new HashSet<>();
	private Set<String> documentID = new HashSet<>();
	private CorpusManager() {}
	
	/**
	 * Restituisce l'istanza di CorpusManager
	 * 
	 * @return l'istanza di CorpusManager
	 */
	public static CorpusManager getInstance() {
		if (instance == null)
			instance = new CorpusManager();
		return instance;
	}

	/**
	 * Carica i file partendo da un percorso
	 * 
	 * @param path percorso di un file
	 * @return una nuova istanza di Document
	 */
	public Document parseDocument(Path path) {
		String ss = "";
		String[] s = null;
		try (BufferedReader br = new BufferedReader(new FileReader(path.toFile()))) {
			s = br.readLine().split("\t");
			while (br.ready()) {
				ss += br.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		Document d = new Document(s[1], s[0], ss);
		documents.add(d);
		return d;
	}

	/**
	 * Carica i file partendo da un percorso
	 * @param id percorso di un file sotto forma di stringa
	 * @return una nuova istanza di Document
	 */
	public Document loadDocument(String id) {
		Document e = null;
		if (!documentID.contains(id)) return null;
		try (FileInputStream br = new FileInputStream(id+".ser")) {
			ObjectInputStream in = new ObjectInputStream(br);
	         e =  (Document) in.readObject();
	         in.close();
	         br.close();
		} 
		  catch (IOException i) {
			i.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		return e;
	}

	/**
	 * Salva il documento in memoria
	 * 
	 * @param document il documento da salvare
	 */
	public void saveDocument(Document document) {
		documentID.add(document.getId());
		try (FileOutputStream br = new FileOutputStream(document.getId()+".ser")) {
			ObjectOutputStream b = new ObjectOutputStream(br);
			b.writeObject(document);
			b.close();
			br.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Iterator<Document> iterator() {
		return documents.iterator();
	}
}
