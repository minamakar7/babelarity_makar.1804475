package it.uniroma1.lcl.babelarity;
import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Classe che si occupa di calcolare la similarita' tra due parole
 * @author Mina
 *
 */
public class BabelLexicalSimilarity implements BabelSimilarity{
	/**
	 * Costruttore del BabelLexicalSimilarity
	 */
	public BabelLexicalSimilarity() {
		Set<Set<String>> documenti =new HashSet<>();
		Path cartellaDocumenti = FileName.CORPUS.getPath();
		File cartella = cartellaDocumenti.toFile();
		File[] files = cartella.listFiles();
		Set<String> paroleDocumento = new HashSet<>();
		if(MiniBabelNet.documenti==null) {
			for (File f : files) {
				FileLoader.load(f.toPath(), paroleDocumento, FileLoader::corpus, MiniBabelNet.stopwords);
				documenti.add(new HashSet<>(paroleDocumento));
				paroleDocumento.clear();
			}
			MiniBabelNet.documenti=documenti;
		}
		
	}
	

	@Override
	public double computeSimilarity(LinguisticObject o1, LinguisticObject o2) {
		Word w1 = (Word) o1;
		Word w2 = (Word) o2;
		MiniBabelNet babelNet = MiniBabelNet.getInstance();
		Set<Set<String>> documenti = MiniBabelNet.documenti;
		Map<String, Double> mappaOccorrenze = new HashMap<>();
		Map<String, Double> vettoreW1 = new HashMap<>();
		Map<String, Double> vettoreW2 = new HashMap<>();
		for(Set<String> paroleDocumento : documenti) {
			if (paroleDocumento.contains(w1.getParola()))
				paroleDocumento.forEach(p -> vettoreW1.merge(p, 1.0, (v, n) -> v+1));
			if (paroleDocumento.contains(w2.getParola()))
				paroleDocumento.forEach(p -> vettoreW2.merge(p, 1.0, (v, n) -> v+1));
			paroleDocumento.forEach(p -> mappaOccorrenze.merge(p, 1.0, (v, n) -> v+1));
		}
		pmi(w1.getParola(),vettoreW1,mappaOccorrenze);
		pmi(w2.getParola(),vettoreW2,mappaOccorrenze);
		return babelNet.cosineSimilarity(vettoreW1,vettoreW2);
	}
	
	
	
	/**
	 * Applica il pmi e modifica il valore associato ad ogni synset
	 * @param word la parola
	 * @param frequenzaComune mappa che contiene la frequenza comune
	 * @param freq mappa che contiene la frequenza
	 * @param fileCount numero file
	 */
	public static void pmi(String word,Map<String,Double> frequenzaComune, Map<String, Double> freq) {
		for (String s : frequenzaComune.keySet()) {
			double numeratore = frequenzaComune.get(s) / MiniBabelNet.documenti.size();
			double primaSomma = freq.get(word) / MiniBabelNet.documenti.size();
			double secondaSomma = freq.get(s) / MiniBabelNet.documenti.size();
			frequenzaComune.put(s, Math.log(numeratore / (primaSomma * secondaSomma)));
		}
	}
	
}
