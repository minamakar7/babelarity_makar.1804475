package it.uniroma1.lcl.babelarity;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
 * Classe che si occupa dei caricamenti dei  file 
 * @author Mina
 *
 */
public class FileLoader {
	/**
	 * Implementazione del consumer a tre parametri
	 * @author Mina
	 *
	 * @param <T> primo parametro
	 * @param <U> secondo parametro
	 * @param <Z> terzo parametro
	 */
	@FunctionalInterface
	public interface TriConsumer<T, U, Z> {
		void accept(T t, U u, Z z);
	}	
	
	/**
	 * si occupa del caricamento di un file
	 * @param file file da aprire
	 * @param struct Struttura sul quale caricare i dati
	 * @param consumer il metodo da utilizzare
	 * @return la mappa
	 */
	public static <T> T load(FileName file, T struct, TriConsumer<T, String, Object[]> consumer, Object... params) {
		return load(file.getPath(), struct, consumer, params);
	}
	/**
	 * si occupa del caricamento di un file
	 * @param file file da aprire
	 * @param struct Struttura sul quale caricare i dati
	 * @param consumer il metodo da utilizzare
	 * @return la mappa
	 */
	public static <T> T load(Path path, T struct, TriConsumer<T, String, Object[]> consumer, Object... params) {
		try (BufferedReader br = new BufferedReader(new FileReader(path.toFile()))) {
			while (br.ready())
				consumer.accept(struct, br.readLine(), params);
		} catch (IOException e) {
			e.printStackTrace();
		
		}
		return struct;
	}
	/**
	 * Aggiunge alla mappa la forma flessa e come chiave il suo lemma 
	 * @param mappa lemmi
	 * @param line riga del file che contiene la forma flessa ed il suo lemma
	 * @param params 
	 */
	public static void lemma(Map<String, String> mappa, String line, Object... params) {
		String[] array = line.split("\t");
		if (!mappa.containsKey(array[0])) mappa.put(array[0],array[1]);	
	}

	/**
	 * Aggiunge alla mappa l'ID del synset e come attributo un insieme contenente le parole associate
	 * @param mappa dove verra' inserito come chiave l'id e come attributo l'insieme delle parole associate
	 * @param line la riga del file
	 * @param params 
	 */
	public static void dictionary(Map<String, Set<String>> mappa, String line, Object... params) {
		String[] array = line.split("\t");
		Set<String> set = new HashSet<>();
		for (int n = 1; n < array.length; n++)
			set.add(array[n]);
		mappa.put(array[0], set);
	}
	/**
	 * Aggiunge all'insieme tutte le parole presenti nella riga tranne gli stopwords
	 * @param paroleDocumento l'insieme dove verranno aggiunte tutte le parole
	 * @param br la riga del file
	 * @param params 
	 */
	@SuppressWarnings("unchecked")
	public static void corpus(Set<String> paroleDocumento, String br, Object... params) {
		Set<String> stopwords = null;
		try {
			stopwords = (Set<String>) params[0];
		} catch (ClassCastException e) {
			throw new RuntimeException("Chiamata al metodo con parametri errati");
		}
		Clear.clears(paroleDocumento, br.toLowerCase(), stopwords);
	}
	/**
	 * Aggiunge alla mappa l'ID del synset e come attributo un insieme contenente le gloss associate
	 * @param mappa Aggiunge alla mappa l'ID del synset e come attributo un insieme contenente tutti i significati
	 * @param line la riga del file
	 * @param params mappa dove verra' inserito come chiave l'id e come attributo l'insieme di tutti i significati
	 */
	public static void gloss(Map<String, Set<String>> mappa, String line, Object... params) {
		dictionary(mappa, line);
	}
	/**
	 * Aggiunge all' insieme uno stopwords alla volta
	 * @param set insieme dove mano a mano verranno inserite le parole stopWords
	 * @param line linea del file
	 * @param params
	 */
	public static void stopwords(Set<String> set, String line, Object... params) {
		set.add(line);
	}
	
	/**
	 * Aggiunge alla mappa l'ID sorgente del synset e come attributo una lista contenente le relazioni associate
	 * @param mappa Aggiunge alla mappa l'ID del synset e come attributo la relazione
	 * @param line la riga del file
	 * @param params una mappa contente come chiavi l'id del synset e come attributo il synset
	 */
	@SuppressWarnings("unchecked")
	public static void relazioni(Map<String, List<Relazione>> mappa, String line, Object... params) {
		String[] array = line.split("\t");
		String idSorgente = array[0];
		Map<String, Synset> map = null;
		try {
			map = (Map<String, Synset>) params[0];
		} catch (ClassCastException e) {
			throw new RuntimeException("Chiamata al metodo con parametri errati");
		}
		if (!mappa.containsKey(idSorgente))
			mappa.put(idSorgente, new ArrayList<>());
		Relazione r = new Relazione(map.get(idSorgente), map.get(array[1]), array[2], array[3]);
		map.get(idSorgente).addRelation(r);
		mappa.get(idSorgente).add(r);
	}
	
	
}