
/**
 * 
 * Contiene le classi per il calcolo di similarita' attraverso la rete semantica
 * di MiniBabelNet
 * 
 * @author Mina
 *
 */
package it.uniroma1.lcl.babelarity;